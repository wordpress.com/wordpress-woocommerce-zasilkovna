<?php
/**
* Plugin Name: WooCommerce Zasilkovna
* Plugin URI: https://andering.github.io/
* Description: WooCommerce Zasilkovna https://www.zasilkovna.cz/
* Version: 1.0.0
* Author: andering@gmail.com
* Author URI: https://andering.github.io/
* GitHub Plugin URI: pavelevap/ceske-sluzby
* License: GPL2
*/


$active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
if ( in_array( 'woocommerce/woocommerce.php',  $active_plugins) ) {

    add_filter( 'woocommerce_shipping_methods', 'add_zasilkovna_shipping_method' );
    function add_zasilkovna_shipping_method( $methods )
    {
        $methods['zasilkovna_shipping_method'] = 'WC_Zasilkovna_Shipping_Method';
        return $methods;
    }

    add_action( 'woocommerce_shipping_init', 'zasilkovna_shipping_method_init' );
    function zasilkovna_shipping_method_init()
    {
       require_once 'class-zasilkovna-shipping-method.php';
       require_once 'class-zasilkovna-json-loader.php';
    }

    add_action( 'woocommerce_review_order_after_shipping', 'zasilkovna_zobrazit_pobocky' );
    function zasilkovna_zobrazit_pobocky()
    {



        if ( preg_match('#^zasilkovna_shipping_method:[0-9]+$#', WC()->session->chosen_shipping_methods[0]) ) {


            $packages = WC()->shipping->get_packages()[0]['rates'][WC()->session->chosen_shipping_methods[0]];
            $packages = new WC_Zasilkovna_Shipping_Method($packages->get_instance_id());
            $branches = $packages->getBranches();
            parse_str($_POST['post_data'],$_post);

            ?>

                <ul class="shipping_methods">
                <select style="width: 98%; margin: 1%; background: white;" name="zasilkovna_branches">
                    <option value="">Vyberte pobočku</option>

                    <?php foreach ( $branches as $pobocka_id => $pobocka ) {
                        $selected = (isset($_post['zasilkovna_branches']) && ($_post['zasilkovna_branches'] == $pobocka_id)) ? 'selected' : '';
                        echo "<option value=\"{$pobocka_id}\" {$selected} >{$pobocka->name}</option>";
                    } ?>

                </select>
                </ul>

            <?php
        }
    }

    add_action( 'woocommerce_checkout_process', 'zasilkovna_overit_pobocku' );
    function zasilkovna_overit_pobocku()
    {
        $_post = $_POST;
        if ( preg_match('#^zasilkovna_shipping_method:[0-9]+$#', $_post["shipping_method"][0]) && !$_post["zasilkovna_branches"] ) {
            wc_add_notice( __( 'If you want use service Zasilkovna for a delivery, you need choose branch first.', 'woocommerce' ), 'error' );
        }
    }


    add_action( 'woocommerce_new_order_item', 'zasilkovna_ulozeni_pobocky', 10, 2 );
    function zasilkovna_ulozeni_pobocky($item_id, $item)
    {
        $_post = $_POST;
        if ( preg_match('#^zasilkovna_shipping_method:[0-9]+$#', $_post["shipping_method"][0]) && $_post["zasilkovna_branches"] ) {
            $item_type = $item->get_type();
            if ( $item_type == 'shipping' ) {
                wc_add_order_item_meta( $item_id, 'zasilkovna_shipping_branch_id', esc_attr( $_post['zasilkovna_branches'] ), true );
            }
        }
    }

    add_action( 'woocommerce_admin_order_data_after_billing_address', 'zasilkovna_objednavka_zobrazit_pobocku' );
    add_action( 'woocommerce_email_after_order_table', 'zasilkovna_objednavka_zobrazit_pobocku' );
    add_action( 'woocommerce_order_details_after_order_table', 'zasilkovna_objednavka_zobrazit_pobocku' );
    function zasilkovna_objednavka_zobrazit_pobocku($order)
    {
        if ( $order->has_shipping_method( 'zasilkovna_shipping_method' ) ) {
            foreach ( $order->get_shipping_methods() as $shipping_item_id => $shipping_item ) {
                $packages = new WC_Zasilkovna_Shipping_Method($shipping_item->get_instance_id());
                $branches = $packages->getBranches();
                $pobocka = wc_get_order_item_meta( $shipping_item_id, 'zasilkovna_shipping_branch_id', true );

                if ( $branches[$pobocka] ) {
                    echo "<div style=\"border: 1px solid #e5e5e5; padding: 9px 15px;\"><strong>Zasilkovna:</strong><br />(id:{$branches[$pobocka]->id}) <a target=\"_blank\" href=\"{$branches[$pobocka]->url}\">{$branches[$pobocka]->url}</a></div>";
                }
            }
        }
    }

}
