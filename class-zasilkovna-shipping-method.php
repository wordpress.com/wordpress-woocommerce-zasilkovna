<?php
class WC_Zasilkovna_Shipping_Method extends WC_Shipping_Method {

    public function __construct($instance_id = 0)
    {

        $this->id = "zasilkovna_shipping_method";
        $this->instance_id = absint( $instance_id );
        $this->method_title = __( 'Zasilkovna Shipping Method', 'woocommerce' );
        $this->method_description = __( 'Zasilkovna Shipping Method', 'woocommerce' );
        $this->supports  = [
            'shipping-zones',
            'instance-settings',
            'instance-settings-modal',
        ];

        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        // Define user set variables
        $this->enabled  = $this->get_option( 'enabled' );
        $this->title     = $this->get_option( 'title' );
        $this->cost     = $this->get_option( 'cost' );
        $this->apikey     = $this->get_option( 'apikey' );
        $this->country     = $this->get_option( 'country' );

        add_action( 'woocommerce_update_options_shipping_' . $this->id, [$this, 'process_admin_options'] );
    }

    public function init_form_fields()
    {
        $this->instance_form_fields = [
            'enabled' => [
                'title'     => __( 'Enable/Disable', 'woocommerce' ),
                'type'       => 'checkbox',
                'label'     => __( 'Enable Zasilkovna Shipping', 'woocommerce' ),
                'default'     => 'yes'
            ],
            'title' => [
                'title'     => __( 'Method Title', 'woocommerce' ),
                'type'       => 'text',
                'description'   => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
                'default'    => __( 'Zasilkovna Shipping', 'woocommerce' ),
            ],
            'cost' => [
                'title'     => __( 'Cost', 'woocommerce' ),
                'type'       => 'price',
                'description'   => __( 'Cost.', 'woocommerce' ),
                'default'    => __( 0, 'woocommerce' ),
            ],
            'apikey' => [
                'title'       => __( 'Api Key', 'woocommerce' ),
                'type'        => 'text',
                'description' => __( 'Api Key', 'woocommerce' ),
                'default'     => '',
            ],
            'country' => [
                'title' => __( 'Destination Country', 'woocommerce' ),
                'type'  => 'select',
                'description'   => __( 'Destination Country', 'woocommerce' ),
                'options' => [
                    'cz' => __( 'Czech Republic', 'woocommerce' ),
                    'sk' => __( 'Slovakia Republic', 'woocommerce' ),
                ],
                'default' => 'cz'
            ]
        ];
    }

    public function is_available( $package )
    {
        return true;
    }

    public function calculate_shipping($package = [])
    {
        // send the final rate to the user.
        $this->add_rate( array(
            'label'   => $this->title,
            'package' => $package,
            'cost'    => $this->cost
        ));
    }

    public function getBranches()
    {
        $jsonLoader = new Zasilkovna_Json_Loader();
        return $jsonLoader->load($this->apikey)[$this->country];
    }

}
