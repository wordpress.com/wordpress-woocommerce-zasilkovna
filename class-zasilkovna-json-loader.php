<?php
class Zasilkovna_Json_Loader {

    function load($apiKey)
    {
        $data = get_transient( "woocommerce-zasilkovna-branches" );

        if (!$data) {
            $url = $this->build($apiKey);
            $result = $this->fetch($url);
            $body = $this->verify($result);
            $json = $this->parse($body);

            $data = [];
            foreach($json->data as $branch) {
                $data[$branch->country][$branch->id] = $branch;
            }
            set_transient( "woocommerce-zasilkovna-branches", $data, 24 * 60 * 60);
        }

        return $data;
    }

    function fetch( $url )
    {

        return wp_remote_get( $url );
    }



    function verify( $result ) {
        if ( is_wp_error( $result ) ) {
            throw new Exception( $result->get_error_message() );
        }

        $code = $result['response']['code'];
        if ( $code != 200 ) {
            throw new Exception( 'Ceske_Sluzby_Json_Loader Failed: Neplatná HTTP reakce ze serveru - ' . $code );
        }

        $body = wp_remote_retrieve_body( $result );
        if ( $body == '' ) {
            throw new Exception( 'Ceske_Sluzby_Json_Loader Failed: Obsah Json je prázdný.' );
        }

        return $body;
    }

    function parse( $body )
    {
        $json = json_decode( $body );
        if ( json_last_error() !== JSON_ERROR_NONE ) {
            throw new Exception( 'Ceske_Sluzby_Json_Loader Failed: Neplatný Json obsah získaný ze serveru - ' . json_last_error() );
        }
        return $json;
    }

    function build( $apiKey )
    {
        return "https://www.zasilkovna.cz/api/v3/{$apiKey}/branch.json";
    }

}
